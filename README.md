# SunnyUI.Common

![SunnyUI.Net](https://images.gitee.com/uploads/images/2020/0627/205841_13e961a1_416720.png "SunnyUI.png")

- Blog:   https://www.cnblogs.com/yhuse
- Gitee:  https://gitee.com/yhuse/SunnyUI
- GitHub: https://github.com/yhuse/SunnyUI
- Nuget:  https://www.nuget.org/packages/SunnyUI/ 
- 帮助文档目录: https://www.cnblogs.com/yhuse/p/SunnyUI_Menu.html

欢迎交流，QQ群： 56829229  (SunnyUI技术交流群)，请给源码项目点个Star吧！！！

#### 介绍
- SunnyUI.Common, 基于 MIT 的 .Net Framework4.0+、.Net Core、.Net5 开源工具类库、扩展类库。
- 源码编译环境：VS2019
- 动态库应用环境：VS2010及以上，.Net Framework 4.0及以上（不包括.Net Framework 4 Client Profile），.Net Core 3.1，.Net 5.0
- **推荐通过Nuget安装：Install-Package SunnyUI.Common，或者通过VS2019的管理Nuget程序包搜索 SunnyUI.Common 安装。** 

#### 支持开源
- 希望SunnyUI对您有用，您的支持也是SunnyUI开源的动力，SunnyUI有你更精彩！
![感谢您的支持](https://images.gitee.com/uploads/images/2020/0524/233620_6685fbbf_416720.png "SunnyUISupport.png")
