﻿/******************************************************************************
 * SunnyUI 开源控件库、工具类库、扩展类库。
 * CopyRight (C) 2012-2022 ShenYongHua(沈永华).
 * QQ群：56829229 QQ：17612584 EMail：SunnyUI@QQ.Com
 *
 * Blog:   https://www.cnblogs.com/yhuse
 * Gitee:  https://gitee.com/yhuse/SunnyUI.git
 * GitHub: https://github.com/yhuse/SunnyUI
 *
 * SunnyUI can be used for free under the GPL-3.0 license.
 * If you use this code, please keep this note.
 * 如果您使用此代码，请保留此说明。
 ******************************************************************************
 * 文件名称: UNullableDictionary.cs
 * 文件说明: 可空字典
 * 当前版本: V3.1
 * 创建日期: 2020-01-01
 *
 * 2020-01-01: V2.2 增加文件说明
******************************************************************************/

using System.Collections.Generic;

namespace Sunny.UI
{
    /// <summary>
    /// 可空字典。获取数据时如果指定键不存在可返回空而不是抛出异常
    /// </summary>
    /// <typeparam name="TKey">The type of the t key.</typeparam>
    /// <typeparam name="TValue">The type of the t value.</typeparam>
    /// <seealso cref="System.Collections.Generic.Dictionary{TKey, TValue}" />
    /// <seealso cref="System.Collections.Generic.IDictionary{TKey, TValue}" />
    public class NullableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IDictionary<TKey, TValue>
    {
        /// <summary>
        /// 获取或设置与指定的属性是否有脏数据。
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>TValue.</returns>
        public new TValue this[TKey item]
        {
            get => TryGetValue(item, out var v) ? v : default(TValue);
            set => base[item] = value;
        }
    }
}