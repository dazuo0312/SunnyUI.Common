﻿/******************************************************************************
 * SunnyUI 开源控件库、工具类库、扩展类库、多页面开发框架。
 * CopyRight (C) 2012-2022 ShenYongHua(沈永华).
 * QQ群：56829229 QQ：17612584 EMail：SunnyUI@QQ.Com
 *
 * Blog:   https://www.cnblogs.com/yhuse
 * Gitee:  https://gitee.com/yhuse/SunnyUI
 * GitHub: https://github.com/yhuse/SunnyUI
 *
 * SunnyUI.Common.dll can be used for free under the MIT license.
 * If you use this code, please keep this note.
 * 如果您使用此代码，请保留此说明。
 ******************************************************************************
 * 文件名称: UHtml.cs
 * 文件说明: HTML相关帮助类
 * 当前版本: V3.1
 * 创建日期: 2020-03-18
 *
 * 2020-03-18: V3.0.2 增加文件说明
******************************************************************************/

namespace Sunny.UI
{
    public static class Html
    {
        public static string DealApiParam(string param, bool dealPercentSign = false)
        {
            if (dealPercentSign) param = param.Replace("%", "%25");
            param = param.Replace(" ", "%20");
            param = param.Replace("+", "%2B");
            param = param.Replace("#", "%23");
            param = param.Replace("&", "%26");
            param = param.Replace("/", "%2F");
            param = param.Replace("?", "%3F");
            param = param.Replace("=", "%3D");
            param = param.Replace("@", "%40");

            param = param.Replace("(", "%28");
            param = param.Replace(")", "%29");
            param = param.Replace(",", "%2C");
            param = param.Replace(":", "%3A");
            param = param.Replace(";", "%3B");
            param = param.Replace("<", "%3C");
            param = param.Replace(">", "%3E");
            param = param.Replace("\\", "%5C");
            param = param.Replace("|", "%7C");
            param = param.Replace("\"", "%22");
            return param;

            /*******************************************************************
            字符 - URL编码值
            
            空格 - %20
            "      - %22
            #     - %23
            %    - %25
            &    - %26
            (      - %28
            )      - %29
            +     - %2B
            ,       - %2C
            /       - %2F
            :       - %3A
            ;       - %3B
            <      - %3C
            =      - %3D
            >      - %3E
            ?       - %3F
            @     - %40
            \       - %5C
            |       - %7C 
            
            URL特殊字符转义 
            URL中一些字符的特殊含义，基本编码规则如下：
            1、空格换成加号(+)
            2、正斜杠(/)分隔目录和子目录
            3、问号(?)分隔URL和查询
            4、百分号(%)制定特殊字符
            5、#号指定书签
            6、&号分隔参数
            
            如果需要在URL中用到，需要将这些特殊字符换成相应的十六进制的值
            + %2B
            / %2F
            ? %3F
            % %25
            # %23
            & %26
            *******************************************************************/
        }
    }
}
