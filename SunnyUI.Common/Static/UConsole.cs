﻿/******************************************************************************
 * SunnyUI 开源控件库、工具类库、扩展类库、多页面开发框架。
 * CopyRight (C) 2012-2022 ShenYongHua(沈永华).
 * QQ群：56829229 QQ：17612584 EMail：SunnyUI@QQ.Com
 *
 * Blog:   https://www.cnblogs.com/yhuse
 * Gitee:  https://gitee.com/yhuse/SunnyUI
 * GitHub: https://github.com/yhuse/SunnyUI
 *
 * SunnyUI.Common.dll can be used for free under the MIT license.
 * If you use this code, please keep this note.
 * 如果您使用此代码，请保留此说明。
 ******************************************************************************
 * 文件名称: UConsole.cs
 * 文件说明: Console扩展类
 * 当前版本: V3.1
 * 创建日期: 2020-08-19
 *
 * 2021-08-19: V3.0.6 增加文件说明
******************************************************************************/

using System;

namespace Sunny.UI
{
    public static class UConsole
    {
        /// <summary>
        /// 将指定的 32 位无符号的整数值的文本表示（后跟当前行的结束符）写入标准输出流。
        /// </summary>
        /// <param name="value">要写入的值。</param>
        public static void WriteConsole(this uint value, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + value);
        }

        /// <summary>
        /// 将指定的 32 位有符号整数值的文本表示（后跟当前行的结束符）写入标准输出流。
        /// </summary>
        /// <param name="value">要写入的值。</param>
        public static void WriteConsole(this int value, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + value);
        }

        /// <summary>
        /// 将指定的单精度浮点值的文本表示形式（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="value">要写入的值。</param>
        public static void WriteConsole(this float value, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + value);
        }

        /// <summary>
        /// 将指定的 Unicode 字符值（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="value">要写入的值。</param>
        public static void WriteConsole(this char value, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + value);
        }

        /// <summary>
        /// 将指定的 System.Decimal 值的文本表示形式（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="value">要写入的值。</param>
        public static void WriteConsole(this decimal value, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + value);
        }

        /// <summary>
        /// 将指定的 64 位有符号整数值的文本表示（后跟当前行的结束符）写入标准输出流。
        /// </summary>
        /// <param name="value">要写入的值。</param>
        public static void WriteConsole(this long value, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + value);
        }

        /// <summary>
        /// 将指定的 Unicode 字符子数组（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="buffer">Unicode 字符的数组。</param>
        /// <param name="index">buffer 中的起始位置。</param>
        /// <param name="count">要写入的字符数。</param>
        public static void WriteConsole(this char[] buffer, int index, int count)
        {
            Console.WriteLine(buffer, index, count);
        }

        /// <summary>
        /// 将指定的双精度浮点值的文本表示形式（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="value">要写入的值。</param>
        public static void WriteConsole(this double value, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + value);
        }

        /// <summary>
        /// 使用指定的格式信息，将指定对象的文本表示形式（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="format">复合格式字符串。</param>
        /// <param name="arg0">要使用 format 写入的第一个对象</param>
        /// <param name="arg1">要使用 format 写入的第二个对象</param>
        /// <param name="arg2">要使用 format 写入的第三个对象</param>
        /// <param name="arg3">要使用 format 写入的第四个对象</param>
        public static void WriteConsole(this string format, object arg0, object arg1, object arg2, object arg3)
        {
            Console.WriteLine(format, arg0, arg1, arg2, arg3);
        }

        /// <summary>
        /// 将指定的字符串值（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="value">要写入的值。</param>
        public static void WriteConsole(this string value, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + value);
        }

        /// <summary>
        /// 使用指定的格式信息，将指定对象的文本表示形式（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="format">复合格式字符串。</param>
        /// <param name="arg0">要使用 format 写入的第一个对象</param>
        public static void WriteConsole(this string format, object arg0)
        {
            Console.WriteLine(format, arg0);
        }

        /// <summary>
        /// 使用指定的格式信息，将指定对象的文本表示形式（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="format">复合格式字符串。</param>
        /// <param name="arg0">要使用 format 写入的第一个对象</param>
        /// <param name="arg1">要使用 format 写入的第二个对象</param>
        public static void WriteConsole(this string format, object arg0, object arg1)
        {
            Console.WriteLine(format, arg0, arg1);
        }

        /// <summary>
        /// 使用指定的格式信息，将指定对象的文本表示形式（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="format">复合格式字符串。</param>
        /// <param name="arg0">要使用 format 写入的第一个对象</param>
        /// <param name="arg1">要使用 format 写入的第二个对象</param>
        /// <param name="arg2">要使用 format 写入的第三个对象</param>
        public static void WriteConsole(this string format, object arg0, object arg1, object arg2)
        {
            Console.WriteLine(format, arg0, arg1, arg2);
        }

        /// <summary>
        /// 使用指定的格式信息，将指定的对象数组（后跟当前行终止符）的文本表示形式写入标准输出流。
        /// </summary>
        /// <param name="format">复合格式字符串。</param>
        /// <param name="arg">要使用 format 写入的第一个对象</param>
        public static void WriteConsole(this string format, params object[] arg)
        {
            Console.WriteLine(format, arg);
        }

        /// <summary>
        /// 将指定布尔值的文本表示形式（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="value">要写入的值。</param>
        public static void WriteConsole(this bool value, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + value);
        }

        /// <summary>
        /// 将指定对象的文本表示形式（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="value">要写入的值。</param>
        public static void WriteConsole(this object value, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + value);
        }

        /// <summary>
        /// 将指定的 Unicode 字符数组（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="buffer">Unicode 字符数组。</param>
        public static void WriteConsole(this char[] buffer, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + buffer);
        }

        /// <summary>
        /// 将指定的 Unicode 字符数组（后跟当前行终止符）写入标准输出流。
        /// </summary>
        /// <param name="buffer">Unicode 字符数组。</param>
        public static void WriteConsole(this Exception exception, string prefix = "")
        {
            Console.WriteLine((prefix.IsValid() ? prefix + ": " : "") + exception.Message);
        }
    }
}